package com.pelipets.callboardapplication.data

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Ad(
    var id: String? = "",
    var title: String? = "",
    var description: String? = "",
    var price: Double? = 0.0,
    var sellerName: String? = "",
    var citi: String? = "",
    var phone: String? = ""
)