package com.pelipets.callboardapplication.activity

import android.os.Bundle
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.pelipets.callboardapplication.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var host: NavHostFragment
    private lateinit var navController: NavController

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                navigateToHomeBoard()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_new_add -> {
                navigateToCreateNewAd()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupNavigation()

    }

    private fun setupNavigation() {
        host = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = host.navController
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.adFragment -> setToolbarTitle(R.string.screen_display_ad_title)
                R.id.addAd -> setToolbarTitle(R.string.screen_add_ad_title)
                R.id.editAd -> setToolbarTitle(R.string.screen_edit_ad_title)
                R.id.boardFragment -> setToolbarTitle(R.string.screen_board_title)
            }

            if (destination.id == R.id.adFragment
                || destination.id == R.id.editAd) {
                bottomMenu.visibility = View.GONE
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
                supportActionBar?.setHomeButtonEnabled(true)
            } else {
                bottomMenu.visibility = View.VISIBLE
                supportActionBar?.setDisplayHomeAsUpEnabled(false)
                supportActionBar?.setHomeButtonEnabled(false)
            }
        }

        bottomMenu.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        if (!navController.popBackStack()) super.onBackPressed()
    }

    private fun setToolbarTitle(titleId: Int) {
        getSupportActionBar()?.title = getString(titleId)
    }

    private fun navigateToHomeBoard() {
        navController.navigate(R.id.boardFragment)
    }

    private fun navigateToCreateNewAd() {
        navController.navigate(R.id.addAd)
    }

}
