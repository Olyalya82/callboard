package com.pelipets.callboardapplication.di

import android.app.Application
import com.pelipets.callboardapplication.controller.Controller.Companion.bindController
import com.pelipets.callboardapplication.controller.ProgressController
import com.pelipets.callboardapplication.controllers.PreferencesController
import javax.inject.Inject

class CallboardApplication : Application() {

    @Inject
    lateinit var preferences: PreferencesController

    @Inject
    lateinit var progressController: ProgressController

    override fun onCreate() {
        instance = this
        appComponent = createComponent()
        getComponent().inject(this)
        super.onCreate()
        bindController(progressController)
    }

    private fun createComponent(): AppComponent =
        DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .databaseModule(DatabaseModule())
            .build()

    companion object {
        private const val LOG_TAG = "ImmigrantsApplication"

        private lateinit var appComponent: AppComponent

        @Volatile
        lateinit var instance: CallboardApplication
            private set

        fun getComponent() = appComponent

    }
}