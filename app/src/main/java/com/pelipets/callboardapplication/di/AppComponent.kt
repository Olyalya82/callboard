package com.pelipets.callboardapplication.di

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf( AppModule::class, DatabaseModule::class))
interface AppComponent {

    fun inject(callboardApplication: CallboardApplication)
}
