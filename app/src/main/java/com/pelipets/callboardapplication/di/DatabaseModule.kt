package com.pelipets.callboardapplication.di

import androidx.annotation.NonNull
import com.pelipets.callboardapplication.repo.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule() {

    @Provides
    @NonNull
    @Singleton
    fun provideRepository() = Repository()

}
