package com.pelipets.callboardapplication.di

import androidx.annotation.NonNull
import com.pelipets.callboardapplication.controller.ProgressController
import com.pelipets.callboardapplication.controllers.PreferencesController
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: CallboardApplication) {

    @Provides
    @NonNull
    @Singleton
    fun provideApplication() = application

    @Provides
    @NonNull
    @Singleton
    fun provideProgressController() = ProgressController()

    @Provides
    @NonNull
    @Singleton
    fun providePreferencesController(application: CallboardApplication) = PreferencesController(application)

}
