package com.pelipets.callboardapplication.controller

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

abstract class Controller {

    private var lifecycle: Lifecycle? = null

    @Suppress("unused")
    private val lifecycleObserver = object : LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun removeActivity() {
            unbind()
        }
    }

    private var _activity: FragmentActivity? = null

    protected val activity get() = _activity

    private fun unbind() {
        _activity = null
        lifecycle?.removeObserver(lifecycleObserver)
        lifecycle = null
        onUnbind()
    }

    protected open fun onBind() {
    }

    protected abstract fun onUnbind()

    private fun bind(activity: FragmentActivity?) {
        unbind()
        this._activity = activity
        if (activity != null) {
            val activityLifecycle = activity.lifecycle
            lifecycle = activityLifecycle
            activityLifecycle.addObserver(lifecycleObserver)
        }
        onBind()
    }

    companion object {
        fun Application.bindController(controller: Controller) {
            registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
                override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                }

                override fun onActivityStarted(activity: Activity) {
                    if (activity is FragmentActivity) {
                        controller.bind(activity)
                    }
                }

                override fun onActivityResumed(activity: Activity) {
                }

                override fun onActivityPaused(activity: Activity) {
                }

                override fun onActivityStopped(activity: Activity) {
                }

                override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
                }

                override fun onActivityDestroyed(activity: Activity) {
                }
            })
        }
    }
}
