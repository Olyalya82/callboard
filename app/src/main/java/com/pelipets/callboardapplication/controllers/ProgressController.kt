package com.pelipets.callboardapplication.controller

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import com.pelipets.callboardapplication.R

class ProgressController : Controller() {

    private var progress: AlertDialog? = null
    private var progressDarkTheme: AlertDialog? = null

    override fun onBind() {
        progress = createProgress(false)
        progressDarkTheme = createProgress(true)
    }

    override fun onUnbind() {
        hideProgress()
        progress = null
        progressDarkTheme = null
    }

    @SuppressLint("InflateParams")
    private fun createProgress(darkTheme: Boolean): AlertDialog? {
        val activity = activity ?: return null
        val adb = AlertDialog.Builder(activity)
        val view = activity.layoutInflater.inflate(R.layout.progress_dialog, null)
        val progressBar = view.findViewById<ProgressBar>(R.id.progressBar)
        progressBar.indeterminateDrawable.setColorFilter(
            Color.parseColor(if (darkTheme) "#D9D9D9" else "#6b000000"),
            android.graphics.PorterDuff.Mode.MULTIPLY
        )
        adb.setView(view)
        return adb.create()
            .apply {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                setCancelable(true)
            }
    }

    fun showProgress() {
        progress?.show()
    }

    fun showProgressDarkTheme() {
        progressDarkTheme?.show()
    }

    fun hideProgress() {
        progress?.dismiss()
        progressDarkTheme?.dismiss()
    }
}
