package com.pelipets.callboardapplication.controllers

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager

class PreferencesController(application: Application) {
    private var prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(application)

    var sellerName: String
        get() = prefs.getString(SELLER_NAME, "")!!
        set(value) {
            prefs.edit().putString(SELLER_NAME, value).apply()
        }

    var city: String
        get() = prefs.getString(CITY, "")!!
        set(value) {
            prefs.edit().putString(CITY, value).apply()
        }

    var phone: String
        get() = prefs.getString(PHONE, "")!!
        set(value) {
            prefs.edit().putString(PHONE, value).apply()
        }

    companion object {
        private const val SELLER_NAME = "seller_name"
        private const val CITY = "city"
        private const val PHONE = "phone"
    }
}
