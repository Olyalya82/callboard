package com.pelipets.callboardapplication.repo

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.pelipets.callboardapplication.data.Ad
import io.reactivex.*


class Repository() {
    private val database = FirebaseDatabase.getInstance()
    private val adsReference = database.reference
    private val countReference = database.getReference(COUNT_REF)

    fun saveAdsCount(count: Int) {
        countReference.setValue(count.toString())
    }

    fun getAdsCount(): Maybe<Int> = Maybe.create(MaybeOnSubscribe<String> { emitter ->

        countReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue(String::class.java)
                emitter.onSuccess(value ?: "")
            }

            override fun onCancelled(error: DatabaseError) {
                emitter.onError(Exception("database error"))
            }
        })
    }).map { it.toInt() }

    fun saveNewAd(
        adId: String,
        title: String,
        description: String,
        price: Double,
        sellerName: String,
        city: String,
        phone: String
    ) {
        val ad = Ad(adId, title, description, price, sellerName, city, phone)
        adsReference.child(ADS).child(adId).setValue(ad)
    }

    fun getAdsById(id: Int): Maybe<Ad> = Maybe.create(MaybeOnSubscribe<Ad> { emitter ->

        countReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue(Ad::class.java)
                emitter.onSuccess(value ?: Ad())
            }

            override fun onCancelled(error: DatabaseError) {
                emitter.onError(Exception("database error"))
            }
        })
    })

    fun getAllAds(): Observable<Ad> = Observable.create(ObservableOnSubscribe<Ad> { emitter ->

        countReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue(Ad::class.java)
                emitter.onNext(value ?: Ad())
            }

            override fun onCancelled(error: DatabaseError) {
                emitter.onError(Exception("database error"))
            }
        })
    })

    companion object {
        const val COUNT_REF = "count"
        const val ADS = "ads"
    }
}